import React, {Component} from 'react'
import ReactDragListView from 'react-drag-listview'

class Demo extends Component {
  constructor(props) {
    super(props);

    const data = [];
    for (let i = 1, len = 7; i < len; i++) {
      data.push({
        title: `Recommendation ${i}`
      });
    }

    this.state = {
      data
    }; 
    

  }

  render() {
    const that = this;
    const dragProps = {
      onDragEnd(fromIndex, toIndex) {
        const data = that.state.data;
        const item = data.splice(fromIndex, 1)[0];
        data.splice(toIndex, 0, item);
        that.setState({ data });
      },
      nodeSelector: 'li',
      handleSelector: 'a'
    };

    /* return (
      <ReactDragListView {...dragProps}>
        <ol style={{listStyle: 'none'}}>
          {this.state.data.map((item, index) => (
            <li key={index} style={{display: 'inline'}}>
              {item.title}
              <a href="#">Drag</a>
            </li>
          ))}
        </ol>
      </ReactDragListView>
    ); */

    return (
      <ReactDragListView {...dragProps}>
      <button className="timeButton" onClick={this.test}>
      <div className="entry">
        <h1>{this.props.title}</h1>
        {this.props.desc}
      </div>
      </button>
       </ReactDragListView>
      )
  }
}

export default Demo