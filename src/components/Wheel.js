import React from "react";

class Wheel_6 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  One() {
    this.props.callbackFromParent(this.props.ops.answers[0].a, this.props.ops.answers[0].next);
  }

  Two() {
    this.props.callbackFromParent(this.props.ops.answers[1].a, this.props.ops.answers[1].next,);
  }

  Three() {
    this.props.callbackFromParent(this.props.ops.answers[2].a, this.props.ops.answers[2].next,);
  }

  Four() {
    this.props.callbackFromParent(this.props.ops.answers[3].a, this.props.ops.answers[3].next,);
  }

  Five() {
    this.props.callbackFromParent(this.props.ops.answers[4].a, this.props.ops.answers[4].next,);
  }

  Six() {
    this.props.callbackFromParent(this.props.ops.answers[5].a, this.props.ops.answers[5].next,);
  }

  render() {
    return (
      <div>
        <h1 id="HEADER">{this.props.ops.question}</h1>

        <div id="menu">
          <div
            id="One"
            className="item1 item"
            onClick={() => {
              this.One();
            }}
          >
            <div className="content">
              <a>{this.props.ops.answers[0].a}</a>
            </div>
          </div>

          <div
            id="Two"
            className="item2 item"
            onClick={() => {
              this.Two();
            }}
          >
            <div className="content">
              <a>{this.props.ops.answers[1].a}</a>
            </div>
          </div>

          <div
            id="Three"
            className="item3 item"
            onClick={() => {
              this.Three();
            }}
          >
            <div className="content">
              <a>{this.props.ops.answers[2].a}</a>
            </div>
          </div>

          <div
            id="Four"
            className="item4 item"
            onClick={() => {
              this.Four();
            }}
          >
            <div className="content">
              <a>{this.props.ops.answers[3].a}</a>
            </div>
          </div>

          <div
            id="Five"
            className="item5 item"
            onClick={() => {
              this.Five();
            }}
          >
            <div className="content">
              <a>{this.props.ops.answers[4].a}</a>
            </div>
          </div>

          <div id="wrapper6">
            <div
              id="Six"
              className="item6 item"
              onClick={() => {
                this.Six();
              }}
            >
              <div className="content">
                <a>{this.props.ops.answers[5].a}</a>
              </div>
            </div>
          </div>

          <div id="center">
            <a />
          </div>
        </div>
      </div>
    );
  }
}

export default Wheel_6;
