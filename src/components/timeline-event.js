import React from 'react';
import { Link } from 'react-router-dom';
import ReactDragListView from 'react-drag-listview';

const DragColumn = ReactDragListView.DragColumn;

export default class TimelineEvent extends React.Component {
	constructor(props) {
	    super(props);
	    console.log(props);
	    let data = [];
	    for (let i = 0; i < this.props.array.length; i++) {
	      data.push(
	      {
	        title: this.props.array[i].title,
	        desc: this.props.array[i].desc
	      }
	      );
	    }

	    this.state = {
	      data
	    };

  	}


	componentWillReceiveProps(nextProps) {
  // You don't have to do this check first, but it can help prevent an unneeded render
	 	this.setState({data: nextProps.array});
		

	};




	render(){
    const that = this;
    const dragProps = {
      onDragEnd(fromIndex, toIndex) {
        const data = that.state.data;
        const item = data.splice(fromIndex, 1)[0];
        data.splice(toIndex, 0, item);
        that.setState({ data });
      },
      nodeSelector: 'button',
      handleSelector: 'button',
      lineClassName: 'entryList'
    };


		return(
		<DragColumn {...dragProps}>
		<div className="bar"/>
		<div className="timeline">
        <ol className="entryList" style={{listStyle: 'none'}}>
          {this.state.data.map((item, index) => (

			<button className="timeButton" >
			<div className="entry">
				<h1>{item.title}</h1>
				{item.desc}
			</div>

			</button>
			))}
			</ol>
			</div>
			</DragColumn>
			

			)
	}
}